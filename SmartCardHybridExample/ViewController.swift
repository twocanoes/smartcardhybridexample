//
//  ViewController.swift
//  SmartCardHybridExample
//
//  Created by Timothy Perfitt on 7/28/21.
//

import Cocoa
import CryptoTokenKit
class ViewController: NSViewController {
    override func viewDidLoad() {
        super.viewDidLoad()

        let driverConfig = TKTokenDriver.Configuration.driverConfigurations

        if let tokenConfig = driverConfig["com.twocanoes.SmartCardHybridExample.SmartCardExampleExtension"] {
            let _ = tokenConfig.addTokenConfiguration(for: "test")

        }

    }

    override var representedObject: Any? {
        didSet {
        // Update the view, if already loaded.
        }
    }


}

