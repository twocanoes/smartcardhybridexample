//
//  TokenDriver.swift
//  SmartCardExampleExtension
//
//  Created by Timothy Perfitt on 7/28/21.
//

import CryptoTokenKit

class TokenDriver: TKSmartCardTokenDriver, TKSmartCardTokenDriverDelegate {

    func tokenDriver(_ driver: TKSmartCardTokenDriver, createTokenFor smartCard: TKSmartCard, aid AID: Data?) throws -> TKSmartCardToken {
        let token = try Token.init(smartCard: nil, aid: nil, configuration: nil, tokenDriver: driver )
        return token
    }
/*
     - (nullable TKToken *)tokenDriver:(TKSmartCardTokenDriver *)driver tokenForConfiguration:(TKTokenConfiguration *)configuration error:(NSError **)error{

         TKSmartCard *smartCard=[[TKSmartCard alloc] init];

         return [[Token alloc] initWithSmartCard:smartCard driver:driver configuration:configuration];
     }

     */
    func tokenDriver(_ driver: TKTokenDriver, tokenFor configuration: TKToken.Configuration) throws -> TKToken {

        let smartcard = TKSmartCard()
        let token = try Token.init(smartCard: smartcard, aid: nil, configuration: configuration, tokenDriver: driver as! TKSmartCardTokenDriver)

        return token
    }


}
