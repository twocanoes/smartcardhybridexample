//
//  Token.swift
//  SmartCardExampleExtension
//
//  Created by Timothy Perfitt on 7/28/21.
//

import CryptoTokenKit

class Token: TKSmartCardToken, TKTokenDelegate {

    init(smartCard: TKSmartCard?, aid AID: Data?, configuration:TKToken.Configuration?, tokenDriver: TKSmartCardTokenDriver) throws {
        let instanceID = "tcsinstance" // Fill in a unique persistent identifier of the token instance.

        guard let smartCard=smartCard else {
            let smartcard = TKSmartCard()

            super.init(smartCard: smartcard, aid: nil, instanceID: instanceID, tokenDriver: tokenDriver)
            return
        }
        super.init(smartCard: smartCard, aid:AID, instanceID:instanceID, tokenDriver: tokenDriver)
        

        self.keychainContents!.fill(with: keyItems())

    }

    func createSession(_ token: TKToken) throws -> TKTokenSession {
        return TokenSession(token:self)
    }
    func keyItems()  -> Array<TKTokenKeychainItem> {

        var keyItems = [TKTokenKeychainItem]()

        let constraint = "NONE"

        let constraints : [ NSNumber : TKTokenOperationConstraint] = [
            TKTokenOperation.readData.rawValue as NSNumber : constraint as TKTokenOperationConstraint,
            TKTokenOperation.decryptData.rawValue as NSNumber : constraint as TKTokenOperationConstraint,
            TKTokenOperation.none.rawValue as NSNumber: constraint as TKTokenOperationConstraint,
            TKTokenOperation.performKeyExchange.rawValue as NSNumber: constraint as TKTokenOperationConstraint,
            TKTokenOperation.signData.rawValue as NSNumber: constraint as TKTokenOperationConstraint
        ]
        guard let pathURL = Bundle.main.url(forResource: "certs", withExtension: "plist") else {
            return []
        }
        let plistData = try! Data(contentsOf: pathURL)
        let certArray = try! PropertyListSerialization.propertyList(from: plistData, options: [], format: nil)

        let certificates = certArray as! Array<String>

        var i = 0
        for currCert in certificates {

            let currCertData = NSData(base64Encoded: currCert, options: NSData.Base64DecodingOptions.ignoreUnknownCharacters)!

            if let certificate = SecCertificateCreateWithData(nil, currCertData as CFData){


                let keychainCertificate = TKTokenKeychainCertificate.init(certificate: certificate, objectID:UUID.init().uuidString)
                i=i+1
                keyItems.append(keychainCertificate!)

                if let certItemKey = TKTokenKeychainKey.init(certificate: certificate, objectID: currCert) {
                    certItemKey.canSign = true
                    certItemKey.canDecrypt = true
                    certItemKey.canPerformKeyExchange = true
                    certItemKey.isSuitableForLogin = true
                    certItemKey.constraints = constraints
                    keyItems.append(certItemKey)
                }

            }

        }

        return keyItems

    }



}
